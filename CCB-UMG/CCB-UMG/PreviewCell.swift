//
//  PreviewCell.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/24/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import Foundation
import UIKit

class PreviewCell: UITableViewCell {
   @IBOutlet var poster: UIImageView!
   @IBOutlet var title: UILabel!
   @IBOutlet var genres: UILabel!
   @IBOutlet var vote: UILabel!
   @IBOutlet var rating: UILabel!
}

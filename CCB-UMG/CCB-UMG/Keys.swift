//
//  Keys.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/24/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import Foundation

struct Keys {
   struct ConfigKeys {
      var lastUpdated: String
      struct ImageKeys {
         var baseURL: String
         var secureBaseURL: String
      }
      var images: ImageKeys
   }
   var config: ConfigKeys
}

//let keys = Keys(config: Keys.ConfigKeys(lastUpdated: "configLastUpdated",
//                                        images: Keys.ConfigKeys.ImageKeys(baseURL: "base_url", secureBaseURL: "secure_base_url")))

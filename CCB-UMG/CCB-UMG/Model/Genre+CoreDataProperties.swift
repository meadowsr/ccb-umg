//
//  Genre+CoreDataProperties.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/25/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//
//

import Foundation
import CoreData


extension Genre {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Genre> {
        return NSFetchRequest<Genre>(entityName: "Genre")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: Int32
    @NSManaged public var movie: Movie?
    @NSManaged public var preview: Preview?

}

//
//  Preview+CoreDataProperties.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 6/4/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//
//

import Foundation
import CoreData


extension Preview {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Preview> {
        return NSFetchRequest<Preview>(entityName: "Preview")
    }

    @NSManaged public var adult: Bool
    @NSManaged public var backdrop_path: String?
    @NSManaged public var id: Int32
    @NSManaged public var overview: String?
    @NSManaged public var popularity: Float
    @NSManaged public var poster_path: String?
    @NSManaged public var previewType: Int16
    @NSManaged public var release_date: String?
    @NSManaged public var sortKey: String?
    @NSManaged public var title: String?
    @NSManaged public var video: Bool
    @NSManaged public var vote_average: Float
    @NSManaged public var vote_count: Int32
    @NSManaged public var genres: NSSet?

}

// MARK: Generated accessors for genres
extension Preview {

    @objc(addGenresObject:)
    @NSManaged public func addToGenres(_ value: Genre)

    @objc(removeGenresObject:)
    @NSManaged public func removeFromGenres(_ value: Genre)

    @objc(addGenres:)
    @NSManaged public func addToGenres(_ values: NSSet)

    @objc(removeGenres:)
    @NSManaged public func removeFromGenres(_ values: NSSet)

}

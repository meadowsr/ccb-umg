//
//  DataStructures.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/23/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import Foundation

struct ConfigImages : Decodable {
   let base_url: String
   let secure_base_url: String
}

struct Configuration : Decodable {
   let images: ConfigImages
}

struct GenreList : Decodable {
   struct Genres : Decodable {
      let id: Int32
      let name: String
   }
   let genres: [Genres]
}

struct ReleaseDates: Decodable {
   let id: Int32 // movieID

   struct ReleaseDateList: Decodable {
      let iso_3661_1: String

      struct ReleaseDates: Decodable {
         let certification: String
         let iso_639_1: String
         let note: String
         let release_date: Date
         let type: Int16 // 1=Premiere, 2=Theatrical (limited); 3=Theatrical; 4=Digital; 5=Physical; 6=TV
      }
   }
   let results: [ReleaseDateList]
}

// MARK: -

struct MovieItem : Decodable {
   let adult: Bool
   let backdrop_path: String?
   let budget: Int64
   let homepage: String?
   let id: Int32
   let imdb_id: String?
   let original_language: String
   let original_title: String
   let overview: String?
   let popularity: Float
   let poster_path: String?
   let release_date: String
   let revenue: Int64
   let runtime: Int32?
   let status: String // Allowed Values: Rumored, Planned, In Production, Post Production, Released, Canceled
   let tagline: String?
   let title: String
   let video: Bool
   let vote_average: Float
   let vote_count: Int32

//   let release_dates: ReleaseDates
//   let mpaa_rating: String
}
typealias MovieList = [MovieItem]

// MARK: -

struct PreviewItem : Decodable {
   let poster_path : String?
   let adult : Bool
   let overview : String
   let release_date : String
   let genre_ids : [Int32]
   let id : Int32
   let original_title : String
   let original_language : String
   let title : String
   let backdrop_path : String?
   let popularity : Float
   let vote_count : Int32
   let video : Bool
   let vote_average : Float
}

struct PreviewResponse : Decodable {
   let page : Int
   let results : [PreviewItem] // Movie List Result Object
   //   let dates : object
   //   let maximum : Date
   //   let minimum : Date
   let total_pages : Int
   let total_results : Int
}

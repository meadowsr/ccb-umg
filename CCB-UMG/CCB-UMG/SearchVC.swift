//
//  SearchVC.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 6/4/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class SearchVC: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate
{
   private let appDelegate = UIApplication.shared.delegate as! AppDelegate
   private var moc: NSManagedObjectContext!

   @IBOutlet var searchText: UITextField!
   @IBOutlet var tableView: UITableView!
   @IBOutlet var spinner: UIActivityIndicatorView!

   private var searchResults: [Preview] = []
   private var lastPageShown: Int = 0, totalPages: Int = 0

   override func viewDidLoad() {
      super.viewDidLoad()

      moc = appDelegate.managedObjectContext
      clearDataStore()

      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissMe))
   }

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)

      searchText.becomeFirstResponder()
   }

   // MARK: -

   fileprivate func clearDataStore() {
      moc.performAndWait {
         let entities = ["Preview"]
         for entity in entities {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            fetchRequest.predicate = NSPredicate(format: "previewType = \(ViewType.search.rawValue)")
            let deleteReqest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
               try moc.execute(deleteReqest)
            } catch {
               print(error)
            }
         }

         do {
            try moc.save()
         }
         catch {
            print(error)
         }
      }
   }

   @objc func dismissMe() {
      clearDataStore()
      self.dismiss(animated: true, completion: nil)
   }

   private func doSearch(_ text: String, from: Int = 1) {
      guard text.count > 0 else {
         spinner.stopAnimating()
         return
      }

      Network().searchFor(movieTitle: text, page: from) { (data, response, error) in
         do {
            if let data = data {
               let tempMOC = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
               tempMOC.parent = self.appDelegate.managedObjectContext

               let movieData = try JSONDecoder().decode(PreviewResponse.self, from: data)
               self.searchResults = []
               for item in movieData.results {
                  let object = NSEntityDescription.insertNewObject(forEntityName: String(describing: Preview.self), into: tempMOC) as! Preview
                  object.previewType = ViewType.search.rawValue
                  object.poster_path = item.poster_path
                  object.adult = item.adult
                  object.overview = item.overview
                  object.release_date = item.release_date

                  let request = NSFetchRequest<Genre>(entityName: String(describing: Genre.self))
                  request.predicate = NSPredicate(format: "id IN %@", item.genre_ids)
                  do {
                     let genres = try tempMOC.fetch(request).sorted(by: { (left, right) -> Bool in
                        guard let l = left.name, let r = right.name else { return true }
                        return l.compare(r) == .orderedAscending
                     })
                     object.genres = NSSet(array: genres)
                  }
                  catch (let e) {
                     print(e)
                  }

                  object.id = Int32(item.id)
                  object.title = item.title
                  object.backdrop_path = item.backdrop_path
                  object.popularity = item.popularity
                  object.vote_count = Int32(item.vote_count)
                  object.video = item.video
                  object.vote_average = item.vote_average

                  for prefix in [ "a ", "an ", "the " ] {
                     if item.title.lowercased().hasPrefix(prefix) {
                        object.sortKey = String(item.title[item.title.index(item.title.startIndex, offsetBy: prefix.count)...])
                        break
                     }
                  }
               }

               self.lastPageShown = movieData.page
               self.totalPages = movieData.total_pages

               do {
                  try tempMOC.save()
               }
               catch (let e) {
                  print("ERROR - cannot save tempMOC parsing Now Playing list: \(e)")
               }

               DispatchQueue.main.async {
                  do {
                     try self.moc.save()
                  }
                  catch (let e) {
                     print("ERROR saving MOC: \(e)")
                  }
                  self.fetchObjects()
                  self.spinner.stopAnimating()
               }
            }
         }
         catch (let e) {
            print("ERROR decoding JSON: \(e)")
         }

      }
   }

   private func fetchObjects() {
      let fetchRequest = NSFetchRequest<Preview>(entityName: String(describing: Preview.self))
      let predicate = NSPredicate(format: "previewType = \(ViewType.search.rawValue)")
      var predicates = [predicate]
      if let text = searchText.text {
         predicates.append(NSPredicate(format: "title CONTAINS[cd] \"\(text)\""))
         fetchRequest.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
      }
      else {
         fetchRequest.predicate = predicate
      }
      do {
         searchResults = try moc.fetch(fetchRequest)
         tableView.reloadData()
      }
      catch (let e) {
         print("\(e)")
      }
   }

   // MARK: - UITextFieldDelegate

   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()

      if let text = textField.text {
         spinner.startAnimating()
         doSearch(text)
      }

      return true
   }

   // MARK: - UITableView

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return searchResults.count
   }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PreviewCell
      guard indexPath.row < searchResults.count else { return cell }
      let item = searchResults[indexPath.row]

      cell.title.text = item.title
      cell.vote.text = ""
      cell.vote.isHidden = true

      if let array = item.genres?.allObjects as? [Genre], array.count > 0 {
         cell.genres.isHidden = false
         let genres = array.sorted(by: { (left, right) -> Bool in
            guard let l = left.name, let r = right.name else { return true }
            return l.compare(r) == .orderedAscending
         })
         let names = genres.map { return $0.name ?? "" }
         cell.genres.text = "\(item.release_date?.components(separatedBy: "-").first ?? "") - \(names.joined(separator: ", "))"
      }
      else {
         cell.genres.isHidden = true
      }

      if let path = item.poster_path, let url = Network().urlFor(image: path) {
         cell.poster.kf.setImage(with: url)
         cell.poster.contentMode = .scaleAspectFit
         cell.poster.isHidden = false
      }
      else {
         cell.poster.isHidden = true
      }

      return cell
   }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      searchText.resignFirstResponder()

      defer {
         tableView.deselectRow(at: indexPath, animated: true)
      }

      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: MovieDetailVC.self)) as? MovieDetailVC else {
         return
      }
      let item = searchResults[indexPath.row]
      vc.movieID = item.id
      vc.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissMe))
      navigationController?.pushViewController(vc, animated: true)
   }

   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      if indexPath.row == searchResults.count-1 {
         if lastPageShown < totalPages {
            doSearch(searchText.text!, from: lastPageShown + 1)
         }
      }
   }
}

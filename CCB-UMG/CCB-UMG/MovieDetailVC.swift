//
//  MovieDetailVC.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/23/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class MovieDetailVC: UIViewController
{
   @IBOutlet var container: UIView!
   @IBOutlet var poster: UIImageView!
   @IBOutlet var movieTitle: UILabel!
   @IBOutlet var mpaa: UILabel!
   @IBOutlet var runtime: UILabel!
   @IBOutlet var genres: UILabel!
   @IBOutlet var overview: UILabel!
   @IBOutlet var rating: UILabel!
   @IBOutlet var votes: UILabel!
   @IBOutlet var released: UILabel!
   @IBOutlet var tagline: UILabel!
   @IBOutlet var imdbLink: UITextView!

   var movieID: Int32 = 0
   var movie: Movie? = nil {
      didSet {
         container.isHidden = false
         updateDisplay()
         self.spinner.stopAnimating()
      }
   }

   private var moc: NSManagedObjectContext! = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext

   @IBOutlet var tableView: UITableView!
   @IBOutlet var spinner: UIActivityIndicatorView!

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)

      container.isHidden = true
      spinner.startAnimating()

      DispatchQueue.global(qos: .userInitiated).async {
         if let movie = self.fetchMovieDetailsBy(id: self.movieID) {
            DispatchQueue.main.async {
               self.movie = movie
            }
         }
         else {
            Network().getMovieDetails(self.movieID, { (data, response, error) in
               self.parseMovieData(data, response: response, error: error)
            })
         }
      }
   }

   fileprivate func fetchMovieDetailsBy(id: Int32) -> Movie? {
      let request = NSFetchRequest<Movie>(entityName: String(describing: Movie.self))
      request.predicate = NSPredicate(format: "id == \(self.movieID)")
      do {
         let movie = try self.moc.fetch(request).first
         return movie
      }
      catch (let error ){
         print("ERROR retrieving movie \(self.movieID): \(error)")
      }

      return nil
   }

   // MARK: -

   fileprivate func parseMovieData(_ data: Data?, response: URLResponse?, error: Error?) {
      let tempMOC = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
      tempMOC.parent = moc

      do {
         if let data = data {
            let movieItem = try JSONDecoder().decode(MovieItem.self, from: data)
            let object = NSEntityDescription.insertNewObject(forEntityName: String(describing: Movie.self), into: tempMOC) as! Movie
            object.adult = movieItem.adult
            object.backdrop_path = movieItem.backdrop_path
            object.budget = movieItem.budget
            object.homepage = movieItem.homepage
            object.id = movieItem.id
            object.imdb_id = movieItem.imdb_id
            object.overview = movieItem.overview
            object.popularity = movieItem.popularity
            object.poster_path = movieItem.poster_path ?? ""
            object.release_date = movieItem.release_date
            object.revenue = movieItem.revenue
            object.runtime = movieItem.runtime ?? 0
            object.status = movieItem.status
            object.tagline = movieItem.tagline
            object.title = movieItem.title
            object.video = movieItem.video
            object.vote_average = movieItem.vote_average
            object.vote_count = movieItem.vote_count
         }

         do {
            try tempMOC.save()
         }
         catch (let e) {
            print("ERROR - cannot save tempMOC parsing Now Playing list: \(e)")
         }

         DispatchQueue.main.async {
            do {
               try self.moc.save()
               self.movie = self.fetchMovieDetailsBy(id: self.movieID)
            }
            catch (let e) {
               print("ERROR saving MOC: \(e)")
            }
         }
      }
      catch (let error) {
         print("ERROR decoding movie details for \(movieID): \(error)")
      }
   }

   fileprivate func updateDisplay() {
      if let path = movie?.poster_path, let url = Network().urlFor(image: path) {
         poster.kf.setImage(with: url)
      }
      else {
         poster.image = nil
      }

      movieTitle.text = movie?.title
      mpaa.text = movie?.rating ?? ""
      mpaa.isHidden = (mpaa.text?.count == 0)

      if let runningTime = movie?.runtime {
         let hours = runningTime / 60
         let minutes = runningTime % 60
         runtime.text = "\(hours)h \(minutes)min"
      }
      else {
         runtime.text = ""
      }
      runtime.isHidden = (runtime.text?.count == 0)

      overview.text = movie?.overview

      if let average = movie?.vote_average {
         rating.text = "\(Float(Int(average * 10)) / 10)"
      }
      else {
         rating.text = "?"
      }
      votes.text = "\(movie?.vote_count ?? 0) votes"

      released.text = ""
      if let releaseDate = movie?.release_date {
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "yyyy-MM-dd"
         if let date = dateFormatter.date(from: releaseDate) {
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .none
            released.text = "Released \(dateFormatter.string(from: date))"
         }
      }

      tagline.text = movie?.tagline

      if let link = movie?.imdb_id, let url = URL(string: "https://www.imdb.com/title/\(link)/") {
         let attr = NSMutableAttributedString(string: "View on ", attributes: [.font : imdbLink.font as Any])
         attr.append(NSAttributedString(string: "IMDb.com", attributes: [.link : url, .font : imdbLink.font as Any]))
         imdbLink.attributedText = attr
      }
      else {
         imdbLink.text = ""
      }
   }

   // MARK: - UITextViewDelegate

   func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
      return true
   }
}

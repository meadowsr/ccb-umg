//
//  MovieListVC.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/21/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import CoreData
import Foundation
import UIKit

import Kingfisher

enum ViewType: Int16 {
   case unknown, nowPlaying = 1, popular, topRated, search
}

class MovieListVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate
{
   private let appDelegate = UIApplication.shared.delegate as! AppDelegate
   private var moc: NSManagedObjectContext!

   @IBOutlet var searchBar: UISearchBar!
   @IBOutlet var tableView: UITableView!
   @IBOutlet var spinner: UIActivityIndicatorView!
   @IBOutlet var noRestultsLbl: UILabel!
   @IBOutlet var buttonsContainer: UIView!
   @IBOutlet var nowPlayingBtn: UIButton!
   @IBOutlet var popularBtn: UIButton!
   @IBOutlet var topRatedBtn: UIButton!
   @IBOutlet var tableFooter: UIView!
   @IBOutlet var loadingText: UILabel!
   @IBOutlet var loadingSpinner: UIActivityIndicatorView!
   @IBOutlet var footerText: UILabel!
   @IBOutlet var footerButton: UIButton!

   @IBOutlet var tableBottomOffset: NSLayoutConstraint!
   private var defaultTableOffset: CGFloat = 0

   private var currentView: ViewType = .unknown
   private var movieList: [Preview] = []
   private var searchList: [Preview] = []

   private var lastPageShown: Int = 0, totalPages: Int = 0

   private var abandonRetry = false

   private var isSearching: Bool {
      get {
         guard let searchText = searchBar.text else { return false }
         return searchText.count > 0
      }
   }

   let refreshControl = UIRefreshControl()

   private var currentKey: String {
      get {
         return currentView == .nowPlaying ? "nowPlayingLastUpdate" : currentView == .popular ? "popularLastUpdate" : "topRatedLastUpdate"
      }
   }

   // MARK: -

   override func viewDidLoad() {
      super.viewDidLoad()

      self.title = "Now Playing"

      moc = appDelegate.managedObjectContext
      moc.performAndWait {
         let entities = ["Preview", "Movie"]
         for entity in entities {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            let deleteReqest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
               try moc.execute(deleteReqest)
            } catch {
               print(error)
            }
         }

         do {
            try moc.save()
         }
         catch {
            print(error)
         }
      }

      refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
      tableView.refreshControl = refreshControl

      switchViews(nowPlayingBtn)

      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchMovies))

      searchBar.isHidden = true
      defaultTableOffset = tableBottomOffset.constant

      let notifier = NotificationCenter.default
      notifier.addObserver(self, selector: #selector(keyboardShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
      notifier.addObserver(self, selector: #selector(keyboardHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
   }

   @objc func keyboardShow(notification: NSNotification) {
      if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
         tableBottomOffset.constant += (keyboardSize.size.height - defaultTableOffset)
      }
   }

   @objc func keyboardHide(notification: NSNotification) {
      tableBottomOffset.constant = defaultTableOffset
   }

   // MARK: -

   func clearCoreDataStore() {
      moc.performAndWait {
         let entities = appDelegate.persistentStoreCoordinator.managedObjectModel.entities.filter({ (entity) -> Bool in
            [String(describing: Preview.self)].contains(entity.name)
         })
         for entity in entities {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.name!)
            let deleteReqest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
               try moc.execute(deleteReqest)
            } catch {
               print(error)
            }
         }

         do {
            try moc.save()
         }
         catch {
            print(error)
         }
      }
   }

   private func createFooter() -> UIView {
      footerText = UILabel(frame: CGRect.zero)
      footerButton = UIButton(type: .system)
      footerButton.setTitle("Load Next Page", for: .normal)
      let stack = UIStackView(arrangedSubviews: [footerText, footerButton])
      stack.axis = .vertical

      let footer = UIView(frame: CGRect.zero)
      footer.addSubview(stack)
      stack.translatesAutoresizingMaskIntoConstraints = false
      footer.addConstraints(
         [NSLayoutConstraint(item: stack, attribute: .centerX, relatedBy: .equal, toItem: footer, attribute: .centerX, multiplier: 1.0, constant: 0.0),
          NSLayoutConstraint(item: stack, attribute: .topMargin, relatedBy: .equal, toItem: footer, attribute: .top, multiplier: 1.0, constant: 8.0),
          NSLayoutConstraint(item: stack, attribute: .bottomMargin, relatedBy: .equal, toItem: footer, attribute: .bottom, multiplier: 1.0, constant: 8.0)]
      )
      return footer
   }

   @IBAction private func loadNextPage() {

   }

   // User has explicitly asked to the latest data
   @objc func refreshData(_ refreshControl: UIRefreshControl) {
      // Clean out the existing items to make room for the new-uns
      for item in movieList {
         moc.delete(item)
      }
      updateMovieList()
      refreshControl.endRefreshing()
   }

   fileprivate func refreshObjects() {
      let lastUpdate = UserDefaults.standard.value(forKey: currentKey)
      if Date().timeIntervalSince(lastUpdate as? Date ?? Date.distantPast) > 18 * 60 * 60 {
         // Been a while since we got this data?  Grab any updates...
         updateMovieList()
         return
      }

      do {
         let request = NSFetchRequest<Preview>(entityName: String(describing: Preview.self))
         request.predicate = NSPredicate(format: "previewType == \(currentView.rawValue)")
         movieList = try moc.fetch(request)

         tableView.isHidden = false
         noRestultsLbl.isHidden = true
         if movieList.count == 0 {
            if abandonRetry == true {
               // If we tried and failed for some reason, don't keep trying ad infinitum.
               abandonRetry = false
//               tableView.isHidden = true
               noRestultsLbl.isHidden = false
               return
            }
            else {
               // No objects?  Go grab 'em from the Intertubes...
               abandonRetry = true // But only automatically retry once, in case there really aren't any.
               updateMovieList()
            }
         }

         sortItems()
      }
      catch (let e) {
         print("ERROR fetching entities: \(e)")
      }

      tableView.reloadData()
   }

   fileprivate func sortItems() {
      switch currentView {
      case .nowPlaying:
         if isSearching {
            searchList = sortByName()
         }
         else {
            movieList = sortByName()
         }

      case .popular:
         if isSearching {
            searchList = sortByPopularity()
         }
         else {
            movieList = sortByPopularity()
         }

      case .topRated:
         if isSearching {
            searchList = sortByVoteAverage()
         }
         else {
            movieList = sortByVoteAverage()
         }

      default:
         break
      }
   }

   private func sortByName() -> [Preview] {
      if isSearching {
         return searchList.sorted(by: { (left, right) -> Bool in
            // Sort by title, ignoring leading articles
            guard var l = left.title?.lowercased(), var r = right.title?.lowercased() else { return false }
            for prefix in [ "a ", "an ", "the " ] {
               if l.hasPrefix(prefix) {
                  l = String(l[l.index(l.startIndex, offsetBy: prefix.count)...])
                  break
               }
            }
            for prefix in [ "a ", "an ", "the " ] {
               if r.hasPrefix(prefix) {
                  r = String(r[r.index(r.startIndex, offsetBy: prefix.count)...])
                  break
               }
            }
            return l.compare(r) == .orderedAscending
         })
      }
      else {
         return movieList.sorted(by: { (left, right) -> Bool in
            // Sort by title, ignoring leading articles
            guard var l = left.title?.lowercased(), var r = right.title?.lowercased() else { return false }
            for prefix in [ "a ", "an ", "the " ] {
               if l.hasPrefix(prefix) {
                  l = String(l[l.index(l.startIndex, offsetBy: prefix.count)...])
                  break
               }
            }
            for prefix in [ "a ", "an ", "the " ] {
               if r.hasPrefix(prefix) {
                  r = String(r[r.index(r.startIndex, offsetBy: prefix.count)...])
                  break
               }
            }
            return l.compare(r) == .orderedAscending
         })
      }
   }

   private func sortByPopularity() -> [Preview] {
      if isSearching {
         return searchList.sorted(by: { (left, right) -> Bool in
            return left.popularity > right.popularity
         })
      }
      else {
         return movieList.sorted(by: { (left, right) -> Bool in
            return left.popularity > right.popularity
         })
      }
   }

   private func sortByVoteAverage() -> [Preview] {
      if isSearching {
         return searchList.sorted(by: { (left, right) -> Bool in
            return left.vote_average > right.vote_average
         })
      }
      else {
         return movieList.sorted(by: { (left, right) -> Bool in
            return left.vote_average > right.vote_average
         })
      }
   }

   @objc func searchMovies() {
      if let vc = self.storyboard?.instantiateViewController(withIdentifier: String(describing: SearchVC.self)) {
         let nc = UINavigationController(rootViewController: vc)
         nc.modalPresentationStyle = .fullScreen
         self.present(nc, animated: true, completion: nil)
      }
   }

   @objc func startSearch() {
      tableView.refreshControl = nil
      searchBar.becomeFirstResponder()
      UIView.animate(withDuration: 0.3) {
         self.searchBar.isHidden = false
         self.buttonsContainer.alpha = 0
         self.buttonsContainer.isHidden = true
      }
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(stopSearch))
   }

   @objc func stopSearch() {
      searchBar.text = ""
      searchBar(searchBar, textDidChange: "")
      searchBar.resignFirstResponder()
      tableView.refreshControl = refreshControl
      UIView.animate(withDuration: 0.3) {
         self.searchBar.isHidden = true
         self.buttonsContainer.alpha = 1
         self.buttonsContainer.isHidden = false
      }
      navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(startSearch))
   }

   @IBAction private func switchViews(_ sender: UIButton) {
      var newView: ViewType = .unknown
      switch sender {
      case nowPlayingBtn:
         newView = .nowPlaying
         title = "Now Playing"
      case popularBtn:
         newView = .popular
         title = "Popular"
      case topRatedBtn:
         newView = .topRated
         title = "Top Rated"
      default:
         print("ERROR: Tried to select an invalid view using button \(sender)")
      }
      if newView == currentView {
         print("selected same view!")
         return
      }
      else {
         currentView = newView
      }

      movieList = []
      tableView.reloadData()

      for button in [nowPlayingBtn, popularBtn, topRatedBtn] {
         button!.titleLabel?.font = UIFont.systemFont(ofSize: button!.titleLabel!.font.pointSize)
      }
      sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: sender.titleLabel!.font.pointSize)

      refreshObjects()
   }

   private func updateMovieList(from page: Int = 1) {
      spinner.startAnimating()
      Network().getMovieList(type: currentView, page: page) { (data, response, error) in
         do {
            if let data = data {
               let tempMOC = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
               tempMOC.parent = self.appDelegate.managedObjectContext

               let movieData = try JSONDecoder().decode(PreviewResponse.self, from: data)
               for item in movieData.results {
                  let object = NSEntityDescription.insertNewObject(forEntityName: String(describing: Preview.self), into: tempMOC) as! Preview
                  object.previewType = self.currentView.rawValue
                  object.poster_path = item.poster_path
                  object.adult = item.adult
                  object.overview = item.overview
                  object.release_date = item.release_date

                  let request = NSFetchRequest<Genre>(entityName: String(describing: Genre.self))
                  request.predicate = NSPredicate(format: "id IN %@", item.genre_ids)
                  do {
                     let genres = try tempMOC.fetch(request).sorted(by: { (left, right) -> Bool in
                        guard let l = left.name, let r = right.name else { return true }
                        return l.compare(r) == .orderedAscending
                     })
                     object.genres = NSSet(array: genres)
                  }
                  catch (let e) {
                     print(e)
                  }

                  object.id = Int32(item.id)
                  object.title = item.title
                  object.backdrop_path = item.backdrop_path
                  object.popularity = item.popularity
                  object.vote_count = Int32(item.vote_count)
                  object.video = item.video
                  object.vote_average = item.vote_average

                  for prefix in [ "a ", "an ", "the " ] {
                     if item.title.lowercased().hasPrefix(prefix) {
                        object.sortKey = String(item.title[item.title.index(item.title.startIndex, offsetBy: prefix.count)...])
                        break
                     }
                  }
               }

               do {
                  try tempMOC.save()
               }
               catch (let e) {
                  print("ERROR - cannot save tempMOC parsing Now Playing list: \(e)")
               }

               DispatchQueue.main.async {
                  self.lastPageShown = movieData.page
                  self.totalPages = movieData.total_pages
                  do {
                     try self.moc.save()
                  }
                  catch (let e) {
                     print("ERROR saving MOC: \(e)")
                  }
                  UserDefaults.standard.setValue(Date(), forKey: self.currentKey)
                  self.spinner.stopAnimating()
                  self.refreshObjects()
                  self.tableView.reloadData()
               }
            }
         }
         catch (let e) {
            print("ERROR decoding JSON: \(e)")
         }
      }
   }

   // MARK: - UISearchBarDelegate

   func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
      searchBar.resignFirstResponder()
   }

   func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      if searchText.count > 0 {
         searchList = movieList.filter({ (item) -> Bool in
            return item.title!.lowercased().contains(searchText.lowercased())
         })
      }
      else {
         searchList = []
      }

      sortItems()
      tableView.reloadData()
   }

   // MARK: - UITableView

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PreviewCell
      let item = isSearching ? searchList[indexPath.row] : movieList[indexPath.row]

      cell.title.text = item.title
      switch currentView {
      case .nowPlaying:
         cell.vote.text = ""
      case .popular:
         let popularity = (Float(Int(item.popularity * 10)) / 10.0)
         cell.vote.text = "\(popularity)"
      case .topRated:
         let average = (Float(Int(item.vote_average * 10)) / 10.0)
         cell.vote.text = "\(average)"
      default:
         break
      }
      cell.vote.isHidden = cell.vote.text?.count ?? 0 == 0

      if let array = item.genres?.allObjects as? [Genre], array.count > 0 {
         cell.genres.isHidden = false
         let genres = array.sorted(by: { (left, right) -> Bool in
            guard let l = left.name, let r = right.name else { return true }
            return l.compare(r) == .orderedAscending
         })
         let names = genres.map { return $0.name ?? "" }
         cell.genres.text = "\(item.release_date?.components(separatedBy: "-").first ?? "") - \(names.joined(separator: ", "))"
      }
      else {
         cell.genres.isHidden = true
      }

      if let path = item.poster_path, let url = Network().urlFor(image: path) {
         cell.poster.kf.setImage(with: url)
         cell.poster.contentMode = .scaleAspectFit
         cell.poster.isHidden = false
      }
      else {
         cell.poster.isHidden = true
      }

      return cell
   }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      searchBar.resignFirstResponder()

      defer {
         tableView.deselectRow(at: indexPath, animated: true)
      }

      let storyboard = UIStoryboard(name: "Main", bundle: nil)
      guard let vc = storyboard.instantiateViewController(withIdentifier: String(describing: MovieDetailVC.self)) as? MovieDetailVC else {
         return
      }
      let item = isSearching ? searchList[indexPath.row] : movieList[indexPath.row]
      vc.movieID = item.id
      navigationController?.pushViewController(vc, animated: true)
   }

   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return isSearching ? searchList.count : movieList.count
   }

   func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      if (isSearching == false) && (indexPath.row == movieList.count-1) {
         if lastPageShown < totalPages {
            updateMovieList(from: lastPageShown + 1)
         }
      }
   }
}

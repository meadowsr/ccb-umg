//
//  ConfigVC.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/23/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import CoreData
import Foundation
import UIKit

class ConfigVC: UIViewController
{
   @IBOutlet var spinner: UIActivityIndicatorView!

   private let appDelegate = UIApplication.shared.delegate as! AppDelegate
   private let tempMOC = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)

   override func viewDidLoad() {
      super.viewDidLoad()

      tempMOC.parent = self.appDelegate.managedObjectContext
   }

   override func viewDidAppear(_ animated: Bool) {
      var timerExpired = false, configUpdated = false, genresUpdated = false

      super.viewDidAppear(animated)

      let finishAndTransition = {
         self.spinner.stopAnimating()
         if timerExpired && configUpdated && genresUpdated {
            self.performSegue(withIdentifier: "transition", sender: nil)
         }
      }

      spinner.startAnimating()
      // Not normally a fan of "splash screens", but this is a reasonable exception
      let _ =  Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
         timerExpired = true
         finishAndTransition()
      }

      let updateMetaData = {
         self.updateConfiguration {
            configUpdated = true
            finishAndTransition()
         }
         self.updateGenres {
            genresUpdated = true
            finishAndTransition()
         }
      }

      let defaults = UserDefaults.standard
      if let lastUpdate = defaults.value(forKey: "configLastUpdated") as? Date {
         if Date().timeIntervalSince(lastUpdate) > 60 * 60 * 24 * 2 {
            // Update every two days (TMDB recommends "every few days")
            updateMetaData()
         }
         else {
            configUpdated = true
            genresUpdated = true
            finishAndTransition()
         }
      }
      else {
         updateMetaData()
      }
   }

   func updateConfiguration(_ completionHandler: @escaping () -> Void) {
      Network().getConfiguration { (data, response, error) in
         do {
            if let data = data {
               let config = try JSONDecoder().decode(Configuration.self, from: data)
               let defaults = UserDefaults.standard
               defaults.set(config.images.base_url, forKey: "base_url")
               defaults.set(config.images.secure_base_url, forKey: "secure_base_url")
//               defaults.set(config.images., forKey: "poster_sizes")
               defaults.set(Date(), forKey: "configLastUpdated")
            }
         }
         catch {
            print("ERROR decoding configuration: \(error)")
         }

         DispatchQueue.main.async {
            completionHandler()
         }
      }
   }

   func updateGenres(_ completionHandler: @escaping () -> Void) {
      Network().getGenres { (data, response, error) in
         do {
            if let data = data {
               let genres = try JSONDecoder().decode(GenreList.self, from: data)
               for genre in genres.genres {
                  if let object = NSEntityDescription.insertNewObject(forEntityName: String(describing: Genre.self), into: self.tempMOC) as? Genre {
                     object.id = genre.id
                     object.name = genre.name
                  }
               }

               do {
                  try self.tempMOC.save()
               }
               catch (let e) {
                  print("ERROR - cannot save tempMOC creating Genres: \(e)")
               }
            }
         }
         catch {
            print("ERROR decoding configuration: \(error)")
         }

         DispatchQueue.main.async {
            completionHandler()
         }
      }
   }
}

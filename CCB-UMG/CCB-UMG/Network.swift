//
//  Network.swift
//  CCB-UMG
//
//  Created by Randall Meadows on 5/21/19.
//  Copyright © 2019 Peak Programming, LLC. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

class Network
{
   private var apiKey = "a0dfcea6c69ae8f85a483920f0bad553"
   private var host = "api.themoviedb.org"
   private var rootURLPath = "/3/"

   // MARK: -

   private func networkRequest(path: String, query: [URLQueryItem]?, completion: @escaping CompletionHandler) {
      var baseQuery: [URLQueryItem] = [URLQueryItem(name: "api_key", value: apiKey),
                                       URLQueryItem(name: "language", value: "en-US"),
                                       URLQueryItem(name: "region", value: "US")]
      if let query = query {
         baseQuery.append(contentsOf: query)
      }
      let fullPath = "\(rootURLPath)\(path)"
      var urlComponents = URLComponents()
      urlComponents.scheme = "https"
      urlComponents.host = host
      urlComponents.path = fullPath
      urlComponents.queryItems = baseQuery

      guard let url = urlComponents.url else {
         completion(nil, nil, nil)
         return
      }

      let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
         if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 429 {
            if let header = httpResponse.allHeaderFields["Retry-After"] as? String, let retry = Double(header) {
               print("RETRY: \(retry)")
               DispatchQueue.main.asyncAfter(deadline: .now() + retry + 0.1, execute: {
                  self.networkRequest(path: path, query: query, completion: completion)
               })
               return
            }
         }
         completion(data, response, error)
      }
      task.resume()
   }

   // MARK: -

   func getGenres(_ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "genre/movie/list", query: nil, completion: { (data, response, error) in
         completionHandler(data, response, error)
      })
   }

   func getConfiguration(_ completionHander: @escaping CompletionHandler) {
      networkRequest(path: "configuration", query: nil) { (data, response, error) in
         completionHander(data, response, error)
      }
   }

   func getMovieDetails(_ movieID: Int32, _ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "movie/\(movieID)",
                     query: [URLQueryItem(name: "append_to_response", value: "release_dates")],
                     completion: { (data, response, error) in
                        completionHandler(data, response, error)
      })
   }

   func getNowPlaying(_ page: Int = 1, _ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "movie/now_playing",
                     query: [URLQueryItem(name: "page", value: "\(page)")],
                     completion: { (data, response, error) in
                        completionHandler(data, response, error)
      })
   }

   func getPopular(_ page: Int = 1, _ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "movie/popular",
                     query: [URLQueryItem(name: "page", value: "\(page)")],
                     completion: { (data, response, error) in
                        completionHandler(data, response, error)
      })
   }

   func getTopRated(_ page: Int = 1, _ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "movie/top_rated",
                     query: [URLQueryItem(name: "page", value: "\(page)")],
                     completion: { (data, response, error) in
                        completionHandler(data, response, error)
      })
   }

   func getMovieList(type: ViewType, page: Int = 1, _ completionHandler: @escaping CompletionHandler) {
      switch type {
      case .nowPlaying:
         getNowPlaying(page) { (data, response, error) in
            completionHandler(data, response, error)
         }
      case .popular:
         getPopular(page) { (data, response, error) in
            completionHandler(data, response, error)
         }
      case .topRated:
         getTopRated(page) { (data, response, error) in
            completionHandler(data, response, error)
         }
      default:
         break
      }
   }

   func searchFor(movieTitle: String, page: Int, _ completionHandler: @escaping CompletionHandler) {
      networkRequest(path: "search/movie",
                     query: [URLQueryItem(name: "page", value: "\(page)"),
                             URLQueryItem(name: "query", value: movieTitle)],
                     completion: { (data, response, error) in
                        completionHandler(data, response, error)
      })
   }

   func urlFor(image: String) -> URL? {
      guard image.count > 0 else { return nil }
      guard let base = UserDefaults.standard.value(forKey: "secure_base_url") as? String else { return nil }
      let url = URL(string: base)?.appendingPathComponent("w500").appendingPathComponent(image)

      return url
   }
}
